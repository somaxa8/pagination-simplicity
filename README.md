# Pagination Simplicity

## Getting started

This library provides you with a clean way to implement your pagination on the controller side, it provides you with a couple of notations that will allow you to both return a page object from the controller side and return a list and capture the get 'page' and 'pageSize' parameters. ' to create a PageRequest object for you

## Examples 

```kt
@Paged
@GetMapping("/examples")
fun getAll(@Pager pageRequest: PageRequest): Page<Example> {
    return exampleService.findAll(pageRequest)
}
```

## Paged 

Paged is an annotation that allows you to return a page object and have the response simply contain a listing, while the metadata will be provided by the response headers.


These are the headers you will receive in the response.

- Pager-Total-Elements: Contains the number of elements found in the database.
- Pager-Total-Pages: Contains the total number of existing pages.
- Pager-Page-Size: Contains the value of the page size.

## Pager 

The function of pager is to capture the get parameters called `page` and `pageSize` and construct a PageRequest object which you can get in the controller handler like this

``` fun getAll(@Pager pageRequest: PageRequest) ```

## License

This project is licensed under the GNU General Public License, Version 3.0 (GPL-3.0).

### GNU General Public License, Version 3.0

This program is free software: you can redistribute it and/or modify it under the terms of the GNU General Public License as published by the Free Software Foundation, either version 3 of the License, or (at your option) any later version.

This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for more details.

You should have received a copy of the GNU General Public License along with this program. If not, see [http://www.gnu.org/licenses/](http://www.gnu.org/licenses/).

### How GPL 3.0 Affects Your Use

- If you use this library or any part of this project in your own project, your project may be required to be licensed under the GPL 3.0 as well.
- You must provide access to the source code of this library or any modified versions of it to anyone who receives your software.
- Any changes you make to this library must be made available under the GPL 3.0.
- If you distribute your software containing this library, you must include a copy of the GPL 3.0 license.
- Please review the full terms of the GPL 3.0 license to ensure compliance with its requirements.

By using this software, you agree to be bound by the terms and conditions of the GNU General Public License, Version 3.0.

If you have any questions or need further clarification regarding the licensing of this project, please contact the project maintainers.
