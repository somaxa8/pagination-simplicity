package com.somaxa8.pagination_simplicity.resolver

import com.somaxa8.pagination_simplicity.annotation.Pager
import com.somaxa8.pagination_simplicity.utils.PageRequestBuilder
import org.springframework.core.MethodParameter
import org.springframework.data.domain.PageRequest
import org.springframework.web.bind.support.WebDataBinderFactory
import org.springframework.web.context.request.NativeWebRequest
import org.springframework.web.method.support.HandlerMethodArgumentResolver
import org.springframework.web.method.support.ModelAndViewContainer

class PageParamResolver : HandlerMethodArgumentResolver {

    override fun supportsParameter(parameter: MethodParameter): Boolean {
        return parameter.parameterAnnotations.any { it is Pager }
    }

    override fun resolveArgument(
        parameter: MethodParameter,
        mavContainer: ModelAndViewContainer?,
        webRequest: NativeWebRequest,
        binderFactory: WebDataBinderFactory?
    ): PageRequest {
        return PageRequestBuilder.build(webRequest)
    }
}