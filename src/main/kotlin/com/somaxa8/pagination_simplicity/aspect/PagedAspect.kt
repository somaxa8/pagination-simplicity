package com.somaxa8.pagination_simplicity.aspect

import jakarta.servlet.http.HttpServletResponse
import org.aspectj.lang.ProceedingJoinPoint
import org.aspectj.lang.annotation.Around
import org.aspectj.lang.annotation.Aspect
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.data.domain.Page
import org.springframework.stereotype.Component


@Aspect
@Component
class PagedAspect {

    @Autowired
    private lateinit var response: HttpServletResponse

    @Around("@annotation(com.somaxa8.pagination_simplicity.annotation.Paged)")
    fun injectValue(joinPoint: ProceedingJoinPoint): Any {
        val result = joinPoint.proceed()

        if (result is Page<*>) {
            response.addHeader("Pager-Total-Elements", result.totalElements.toString())
            response.addHeader("Pager-Total-Pages", result.totalPages.toString())
            response.addHeader("Pager-Page-Size", result.size.toString())
        }

        return result
    }

}