package com.somaxa8.pagination_simplicity

import org.springframework.boot.autoconfigure.SpringBootApplication
import org.springframework.boot.runApplication

@SpringBootApplication
class PaginationSimplicityApplication

fun main(args: Array<String>) {
	runApplication<PaginationSimplicityApplication>(*args)
}
