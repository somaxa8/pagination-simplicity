package com.somaxa8.pagination_simplicity.utils

import org.springframework.data.domain.PageRequest
import org.springframework.web.context.request.NativeWebRequest

object PageRequestBuilder {

    fun build(request: NativeWebRequest): PageRequest {

        val page = request.getParameter("page")?.toIntOrNull() ?: 0
        val pageSize = request.getParameter("pageSize")?.toIntOrNull() ?: 20

        return PageRequest.of(page, pageSize)
    }
}